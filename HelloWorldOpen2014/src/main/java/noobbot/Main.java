package main.java.noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.google.gson.Gson;

public class Main {
    public static void main(String... args) throws IOException {
    	for(int i = 0; i < args.length; i++){
    		System.out.println("arg" + i + ": " + args[i]);
    	}
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;


    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;
        ArrayList<TrackPiece> trackPieces = new ArrayList<>();
        ArrayList<Car> cars = new ArrayList<>();
        Track track = null;
        JSONParser parser = new JSONParser();
        Object obj;
        JSONObject json_obj = new JSONObject();
        JSONArray json_array;
        
        boolean wantToSwitch = false;
        double desiredVelocity = 1337.0;
        double throttle = 1.0;
        double friction = 0.98;
        double acceleration = 0.2;
        double lastRatio, ratio = 0;
        Car ourCar = new Car(0,0,0,0,"Team NTNU","red");
        
        send(join);

        while((line = reader.readLine()) != null) {
        	wantToSwitch = false;
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            try{
            	//final JSONObject = msgFrom
            	//System.out.println("GOT JSON MESSAGE: " + line);
            obj = parser.parse(line);
            if(obj instanceof JSONObject){
            	json_obj = (JSONObject)obj;
            	//System.out.println("GOT JSON OBJ: " + json_obj);
            	//System.out.println("GOT JSON OBJ(2): " + json_obj.get("data"));
            }
            else if(obj instanceof JSONArray){
                json_array = (JSONArray)obj;
            	System.out.println("GOT JSON ARRAY: " + json_array);
            	System.exit(-1);
            }
            else{
            	System.out.print("Unknown JSON type...");
            	System.exit(-1);
            }
        	//System.out.println("GOT JSON OBJ, " + (json_obj.getClass()) + ": " + json_obj);
            } catch (Exception e){
            	System.out.println("couldn't parse server message:(");
            	e.printStackTrace();
            	continue;
            }
           // System.out.println("GOT JSON ARRAY: " + json_array);
            if (msgFromServer.msgType.equals("carPositions")) {
                JSONArray data = (JSONArray)json_obj.get("data");
                JSONObject currentData;
            	JSONObject json_id;
                for(int i = 0; i < data.size(); i++){
                	//treat each car now
                	currentData = (JSONObject) data.get(i);
                	json_id = (JSONObject) currentData.get("id");
                	for(int j = 0; j < cars.size(); ++j){
                		if(cars.get(j).color.equals((String)json_id.get("color"))){
                			//found the correct car
                			cars.get(j).updateStats(currentData);
                			System.out.println(cars.get(j).getStatus());
                			continue;
                		}
                			
                	}
                }
                //decide throttle
                double velocity = ourCar.velocity;
                //strategy for desiredVel:
                //find length to next turn
                //find radius of turn
                //find max allowed vel when entering turn
                //find brake distance from friction
                //keep speed within brake distance
                double nextTurnIn = track.getDistToTurn(ourCar.pieceIndex, ourCar.inPieceDistance, ourCar.lane);
                double nextRadius = track.getRadiusNextTurn(ourCar.pieceIndex, ourCar.lane);
                double maxAllowedVel = nextRadius*0.06;	//this needs investigation. Maybe it's not even linear:/
                double brakeDistance = ourCar.getBrakeDist(maxAllowedVel, friction);
                double nextBrakeDistance = ourCar.getNextBrakeDist(maxAllowedVel, friction);
                System.out.println("maxAllowedVel: " + maxAllowedVel + ", nextTurnIn: " + nextTurnIn + ", brakeDistance: " + brakeDistance + ", nextBrakeDistance: " + nextBrakeDistance);
                if(nextBrakeDistance < nextTurnIn){
                	//In this case just gas on
                	desiredVelocity = 1337;
                }
                //else if(brakeDistance < nextTurnIn){
                //	
                //}
                else{
                	//very corner cutting here...
                	desiredVelocity = maxAllowedVel;
                }
                if(velocity*friction < (desiredVelocity-acceleration))
                	throttle = 1.0;
                else if(velocity > (desiredVelocity*friction+acceleration))
                	throttle = 0;
                else{
                	throttle = 5*acceleration*((desiredVelocity-velocity*friction)/acceleration);
	                if(throttle < 0.0)
	                	throttle = 0.0;
	                if(throttle > 1.0)
	                	throttle = 1.0;
                }
                //emergencies...
                if(ourCar.angle > 57.0 || ourCar.angle < -57.0)
                	throttle = 0;
                	//throttle = desiredVelocity/10.0;
                System.out.println("DesiredVel: "+ desiredVelocity + ", vel: " + ourCar.velocity + ", sending throttle of " +throttle);
                System.out.println("is on piece: " + track.pieces.get((int) ourCar.pieceIndex).toString2((int) ourCar.lane) + ", inPiece: " + ourCar.inPieceDistance);

        		double inPiece = ourCar.inPieceDistance;
        		double length = track.pieces.get((int) ourCar.pieceIndex).getLength((int) ourCar.lane);
        		lastRatio = ratio;
        		ratio = inPiece/length;
        		double flagAngle = (ratio-lastRatio)*(45*Math.PI/180);
                double torque = 20*(throttle/5)*Math.sin(flagAngle)/60;
                double anglePrediction = (Math.PI*ourCar.angle/180 + torque*ourCar.momentOfInertia)*180/Math.PI;
                System.out.println("Predicting next angularVelocity = " + anglePrediction);
                send(new Throttle(throttle));
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
                JSONObject race = (JSONObject)((JSONObject)json_obj.get("data")).get("race");
                JSONObject json_track = (JSONObject)race.get("track");
                JSONArray pieces = (JSONArray)json_track.get("pieces");
                for(int i = 0; i < pieces.size(); i++){
                trackPieces.add(TrackPiece.pieceFromJson((JSONObject)pieces.get(i)));
                }
                JSONArray json_cars = (JSONArray)race.get("cars");
                for(int i = 0; i < json_cars.size(); i++){
                	cars.add(Car.pieceFromJson(i,(JSONObject)json_cars.get(i)));
        			if(ourCar.name.equals(cars.get(i).name))
        				ourCar = cars.get(i);
                }
                //System.out.println("Track first 3: " + trackPieces);
                System.out.println("Cars: " + cars);
                JSONArray json_lanes = (JSONArray) json_track.get("lanes");
                ArrayList<Long> lanes = new ArrayList<>();
                for(int i = 0; i < json_lanes.size(); i++){
                	lanes.add((Long) ((JSONObject)json_lanes.get(i)).get("distanceFromCenter"));
                }
                JSONObject json_startPoint = (JSONObject) json_track.get("startingPoint");
                JSONObject json_position = (JSONObject) json_startPoint.get("position");
                track = new Track((String)json_track.get("name"),trackPieces,lanes,(double)json_position.get("x"),(double)json_position.get("y"),(double)json_startPoint.get("angle"));
                Car.track = track;
                TrackPiece.track = track;
                System.out.println(track); 
               // track.add(json_obj.get("pieces"));
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
                //System.out.println(track.pieces);
            } else {
                send(new Ping());
            }
        }
        for(Car car : cars){
        	System.out.println(car);
        	System.out.println(car.review());
        }
        System.out.println("Best total time to beat: 22:04");
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}