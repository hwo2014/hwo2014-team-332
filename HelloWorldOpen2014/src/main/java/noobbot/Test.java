package main.java.noobbot;

public class Test {
	public static void main(String[] args){
		double inPiece = 5.285920834739102;
		double length = 86.39379797371932;
		double ratio = inPiece/length;
		double radius = 20;
		double a = 0.343362587058369/5;
		double flagAngle = ratio*(45*Math.PI/180);
		double torque = radius*a*Math.sin(flagAngle)/60;
		System.out.println("Track angle: " + (45*Math.PI/180));
		System.out.println("Flag angle: " + flagAngle);
		System.out.println("Flag angle (degrees): " + (180*flagAngle/Math.PI));
		System.out.println("Torque: " + torque);
		
		System.out.println("Desired: " + (0.10234101635536404*Math.PI/180));

		System.out.println(12.929697975827976/0.06654555572151644);
		System.out.println(13.645612791723863/0.04547231467966917);
		//Goal angle: 0.10234101635536404
		
		//Cars: [Car Team NTNU, color: red, width/length/guideFlagPosition: 20.0/40.0/0.0]
		
		/*Team NTNU: velocity: 6.256456614886048, angle: 0.0, pos: 0.0
		DesiredVel: 6.2, vel: 6.256456614886048, sending throttle of 0.343362587058369
		is on piece: turning trackpiece of length 86.39379797371932, radius: 110.0, angle: 45.0
		, inPiece: 5.285920834739102
		Team NTNU: velocity: 6.238118063466159, angle: 0.10234101635536404, pos: 0.0
		DesiredVel: 6.2, vel: 6.238118063466159, sending throttle of 0.4332214890158248
		is on piece: turning trackpiece of length 86.39379797371932, radius: 110.0, angle: 45.0
		, inPiece: 11.52403889820526*/
		
		/*Predicting next angularVelocity = 6.519691626135121
Team NTNU: velocity: 6.2193892723297175, angle: 6.7011517300838515, pos: 0.0, angularVelocity: 0.4473646634024453
DesiredVel: 6.2, vel: 6.2193892723297175, sending throttle of 0.5249925655843857
is on piece: turning trackpiece of length 86.39379797371932, radius: 110.0, angle: 45.0
, inPiece: 24.246045311457276
Predicting next angularVelocity = 6.885234327868183
Team NTNU: velocity: 6.24786961732347, angle: 7.1417612071555325, pos: 0.0, angularVelocity: 0.4406094770716811
				*/
	}
}
