package main.java.noobbot;

import java.util.ArrayList;

public class Track {
	ArrayList<TrackPiece> pieces;
	ArrayList<Long> lanes;
	double startX,startY,startAngle;
	String name;
	
	public Track(String name,ArrayList<TrackPiece> pieces,ArrayList<Long> lanes,double startX,double startY,double startAngle){
		this.pieces = pieces;
		this.lanes = lanes;
		this.startX = startX;
		this.startY = startY;
		this.startAngle = startAngle;
		this.name = name;
	}
	
	public void addPiece(TrackPiece p){
		pieces.add(p);
	}
	
	public double getDistToTurn(int pieceIndex,double pieceDistance,int lane){
		//TrackPiece p = getNextPiece(pieceIndex);
		TrackPiece p = pieces.get(pieceIndex);
		if(p.isTurn)
			return 0.0;
		double dist = pieces.get(pieceIndex).getLength(lane)-pieceDistance;
		p = getNextPiece(pieceIndex);
		while(!p.isTurn){
			dist += p.getLength(lane);
			pieceIndex++;
			p = getNextPiece(pieceIndex);
		}
		return dist;
	}
	
	public double getRadiusNextTurn(int pieceIndex,int lane){
		//TrackPiece p = getNextPiece(pieceIndex);	//nonono
		TrackPiece p = pieces.get(pieceIndex);
		while(!p.isTurn){
			pieceIndex++;
			p = getNextPiece(pieceIndex);
		}
		return p.getRadius(lane);
	}
	
	public TrackPiece getNextPiece(int pieceIndex){
			return pieces.get((pieceIndex+1) % pieces.size());
	}
	
	public String toString(){
		String str = "Track " + name + " of " + pieces.size() + " pieces, ";
		str += "startX: " + startX + ", startY: " + startY + ", angle: " + startAngle;
		return str;
	}
}
