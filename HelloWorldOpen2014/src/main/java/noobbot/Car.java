package main.java.noobbot;

import org.json.simple.JSONObject;

public class Car {
	double velocity,position;
	String color, name;
	int id, lap, frames;
	int pieceIndex,lane;
	int lastPieceIndex,lastLane;
	double width,length, guideFlagPosition, posX, posY,inPieceDistance,angle;
	double lastPosX,lastPosY,lastInPieceDistance,lastAngle,angularVelocity;
	double maxAngle,minAngle;
	double averageVelocity, velocitySum,maxVelocity;
	double momentOfInertia;
	static Track track;
	
	public Car(int id,double width, double length, double guideFlagPosition, String name, String color){
		this.id = id;
		this.name = name;
		this.color = color;
		this.width = width;
		this.length = length;
		maxAngle = -999;
		minAngle = 999;
		velocitySum = 0.0;
		momentOfInertia = 0.0017861876952384695/0.0010995690995754266;
	}
	
	public static Car pieceFromJson(int id,JSONObject obj){
		JSONObject json_id = (JSONObject) obj.get("id");
		JSONObject json_dimensions = (JSONObject) obj.get("dimensions");
		return new Car(id,(double)json_dimensions.get("width"),(double)json_dimensions.get("length"),(double)json_dimensions.get("guideFlagPosition"),(String)json_id.get("name"),(String)json_id.get("color"));
	}
	
	public void updateStats(JSONObject data){
		lastPieceIndex = pieceIndex;
		lastInPieceDistance = inPieceDistance;
		lastAngle = angle;
		lastLane = lane;
		JSONObject piecePosition = (JSONObject) data.get("piecePosition");
		JSONObject json_lane = (JSONObject) piecePosition.get("lane");
		pieceIndex = (int)(long) piecePosition.get("pieceIndex");
		inPieceDistance = (double) piecePosition.get("inPieceDistance");
		angle = (double) data.get("angle");
		if(angle > maxAngle)
			maxAngle = angle;
		if(angle < minAngle)
			minAngle = angle;
		lane = (int)(long)json_lane.get("endLaneIndex");
		if(pieceIndex == lastPieceIndex)
			velocity = inPieceDistance-lastInPieceDistance;
		else{
			//fuk dis shit
			TrackPiece lastP = track.pieces.get((int) lastPieceIndex);
			TrackPiece currentP = track.pieces.get((int) pieceIndex);
			double lastLength = lastP.getLength((int)lastLane);
			velocity = inPieceDistance + (lastLength-lastInPieceDistance);
		}
		angularVelocity = angle-lastAngle;
		velocitySum += velocity;
		if(velocity > maxVelocity)
			maxVelocity = velocity;
		frames++;
	}
	
	public String getStatus(){
		return name + ": velocity: " + velocity + ", angle: " + angle + ", pos: " + guideFlagPosition + ", angularVelocity: " + angularVelocity;
	}
	
	public String toString(){
		return "Car " + name + ", color: " + color + ", width/length/guideFlagPosition: " + width + "/" + length + "/" + guideFlagPosition;
	}
	
	public String review(){
		averageVelocity = velocitySum/frames;
		return "Max velocity: " + maxVelocity + ", averageVelocity: " + averageVelocity + ", minAngle: " + minAngle + ", maxAngle: " + maxAngle + ", finishTime: " + (frames/60.0);
	}
	
	public double getBrakeDist(double maxAllowedVel,double friction){
        //s = (2(v0(v1-v0)+(v1-v0)^2)/2a
        double v1 = maxAllowedVel;
        double v0 = velocity;
        double v1_v0 = v1-v0;
        double a = -v0*(1-friction);
        double brakeDistance = (2*v0*v1_v0+v1_v0*v1_v0)/(2*a);
        return brakeDistance;
	}
	
	//Check if pedal to the metal is a good idea...
	public double getNextBrakeDist(double maxAllowedVel,double friction){
        //s = (2(v0(v1-v0)+(v1-v0)^2)/2a
        double v1 = maxAllowedVel;
        double v0 = velocity+(1-friction)*10;
        double a = -v0*(1-friction);
        double v1_v0 = v1-v0;
        double brakeDistance = (2*v0*v1_v0+v1_v0*v1_v0)/(2*a)-v0;
        return brakeDistance;
		
	}
	
}
