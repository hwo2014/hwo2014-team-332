package main.java.noobbot;

import org.json.simple.JSONObject;

public class TrackPiece {
	double length, radius, angle;
	//angle > 0: turn with clock.
	//angle < 0: turn against clock.
	boolean isSwitch, isTurn;
	static Track track;
	
	public TrackPiece(boolean isSwitch){
		this.isSwitch = isSwitch;
	}
	
	public void setTurn(double radius, double angle){
		isTurn = true;
		this.radius = radius;
		this.angle = angle;
	}
	
	public void setLength(double length){
		this.length = length;
	}
	
	public static TrackPiece makeTrackPiece(double length, boolean isSwitch){
		TrackPiece piece = new TrackPiece(isSwitch);
		piece.setLength(length);
		return piece;
	}
	
	public static TrackPiece makeTrackPiece(double radius, double angle, boolean isSwitch){
		TrackPiece piece = new TrackPiece(isSwitch);
		piece.setTurn(radius,angle);
		return piece;
	}
	
	public double getLength(){
		double l;
		if(isTurn)
			l = radius*angle*Math.PI/180;
		else
			l = length;
		if(l < 0)
			l *= -1;
		return l;
	}
	
	public double getLength(int laneIndex){
		double l;
		if(isTurn)
			l = getRadius(laneIndex)*angle*Math.PI/180;
		else
			l = length;
		if(l < 0)
			l *= -1;
		return l;
	}
	
	public double getRadius(int laneIndex){
		return radius - track.lanes.get(laneIndex);
	}
	
	public static TrackPiece pieceFromJson(JSONObject obj){
		double length, radius, angle;
		boolean isSwitch = false;
		TrackPiece piece = null;
		if(obj.get("switch") != null)
			isSwitch = (boolean)obj.get("switch");
		if(obj.get("length") != null){
			length = (double)obj.get("length");
			piece = makeTrackPiece(length,isSwitch);
		}
		if(obj.get("radius") != null){
			radius = (long)obj.get("radius");
			angle = (double)obj.get("angle");
			piece = makeTrackPiece(radius,angle,isSwitch);
		}
		return piece;
	}
	
	public String toString(){
		String str = "";
		if(isSwitch)
			str = "switched ";
		if(isTurn)
			str += "turning trackpiece of length " + getLength() + ", radius: " + radius + ", angle: " + angle + "\n";
		else
			str += "trackpiece of length: " + length + "\n";
		return str;
	}
	
	public String toString2(int laneIndex){
		String str = "";
		if(isSwitch)
			str = "switched ";
		if(isTurn)
			str += "turning trackpiece of length " + getLength(laneIndex) + ", radius: " + getRadius(laneIndex) + ", angle: " + angle + "\n";
		else
			str += "trackpiece of length: " + length + "\n";
		return str;
	}
}
